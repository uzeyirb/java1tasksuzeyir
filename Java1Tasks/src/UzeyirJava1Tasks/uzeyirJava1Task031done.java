package UzeyirJava1Tasks;

public class uzeyirJava1Task031done {
//In your framework, share your code that uses try-with-resources
	public static void main(String[] args) {
		try {
			// create resource statements
		} catch (Exception e) {
			// Handle the Exceptions
		} finally {
			//if resource is not null
			try{
				//close the resources
			}catch (Exception resourceClosingExp){
			}
		}
	}

}
