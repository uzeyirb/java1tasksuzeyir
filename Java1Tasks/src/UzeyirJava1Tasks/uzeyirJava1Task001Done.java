package UzeyirJava1Tasks;

public class uzeyirJava1Task001Done {

	static class AccessPrinterPageStatus {
		int numberOfPages; // default access //a
		public int pagesPrinted; // public access //b
		private int blackWhitePages; // private access // c
		protected int colorPages; // protected access // e

		void setBlackWhitePages(int i) {
			blackWhitePages = i;
		}

		public int getBlackWhitePages() {
			return blackWhitePages;
		}

		public int getColorPages() {
			return colorPages;
		}

		public void setColorPages(int i) {
			this.colorPages = i;
		}

		public static void main(String[] args) {
			// Task001
			// In your framework, use default, private,
			// public, protected modifiers and write the locations of them
			AccessPrinterPageStatus pageStatus = new AccessPrinterPageStatus();

			// Default and public may be accessed directly

			pageStatus.numberOfPages = 10;
			pageStatus.pagesPrinted = 100;
			pageStatus.setBlackWhitePages(100);

			pageStatus.setColorPages(200);

		}
	}

}
