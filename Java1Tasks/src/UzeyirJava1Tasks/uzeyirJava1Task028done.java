package UzeyirJava1Tasks;

import java.util.Arrays;

public class uzeyirJava1Task028done {
//In your framework, share your code that sorts an int array
	 public static void main(String[] args) 
	    { 
	        // Our arr contains 8 elements 
	        int[] arr = {13, 7, 6, 45, 21, 9, 101, 102}; 
	  
	        Arrays.sort(arr); 
	  
	        System.out.println(Arrays.toString(arr)); 
	    } 
}
