package UzeyirJava1Tasks;

public class uzeyirJava1Task014Done {

	
		// In your framework, share your code that has Abstraction through Inheritance class

	public abstract class Car {
		   private String make;
		   private String model;
		   private int year;

		
		   public Car(String make, String model, int year) {
			super();
			this.make = make;
			this.model = model;
			this.year = year;
		}

		public int computeMPG() {
		     System.out.println("Inside Car calculateMPG");
		     return 0;
		   }

		public String getMake() {
			return make;
		}

		public void setMake(String make) {
			this.make = make;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public int getYear() {
			return year;
		}

		public void setYear(int year) {
			this.year = year;
		}
		   
		   
		  
		}
	
	public class MPG extends Car {
		   private int MPG;   // Annual salary
		   
		   public MPG(String make, String model, int year, int MGP) {
		      super(make, model, year);
		      setMPG(MPG);
		   }

		private void setMPG(int mPG2) {
			// TODO Auto-generated method stub
			
		}

		
}
	
}
