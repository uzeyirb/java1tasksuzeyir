package UzeyirJava1Tasks;

public class uzeyirJava1Task016Done {

		public class Animal {

		    private String name;
		    private int size;
		    private int weight;

		    public Animal(String name, int brain, int body, int size, int weight) {
		        this.name = name;
		        this.size = size;
		        this.weight = weight;
		    }

		    public void eat() {
		        System.out.println("Animal.eat() called");

		    }

		    public void move(int speed) {
		        System.out.println("Animal.move() called.  Animal is moving at " +speed);

		    }



		    public String getName() {
		        return name;
		    }


		    public int getSize() {
		        return size;
		    }

		    public int getWeight() {
		        return weight;
		    }
	}
		public class Dog extends Animal {

		    private int eyes;
		    private int legs;
		    private int tail;
		   

		    public Dog(String name, int size, int weight, int eyes, int legs, int tail, int teeth, String coat) {
		        super(name, 1, 1, size, weight);
		        this.eyes = eyes;
		        this.legs = legs;
		        this.tail = tail;
		        
		    }

		    private void chew() {
		        System.out.println("Dog.chew() called");
		    }

		    @Override
		    public void eat() {
		        System.out.println("Dog.eat() called");
		        chew();
		        super.eat();
		    }

		    public void walk() {
		        System.out.println("Dog.walk() called");
		        super.move(5);
		    }

		    public void run() {
		        System.out.println("Dog.run() called");
		        move(10);

		    }

		    private void moveLegs(int speed) {
		        System.out.println("Dog.moveLegs() called");
		    }
		    @Override
		    public void move(int speed) {
		        System.out.println("Dog.move() called");
		        moveLegs(speed);
		        super.move(speed);
		    }
		}
}
