package UzeyirJava1Tasks;

import java.util.Enumeration;
import java.util.Vector;

public class uzeyirJava1Task026Done {
//In your framework, share your code that uses iterator
	 public static void main(String[] args) 
	    { 
	        // Create a vector and print its contents 
	        Vector vForVector = new Vector(); 
	        for (int i = 0; i < 10; i++) 
	        	 vForVector.addElement(i); 
	        System.out.println( vForVector); 
	  
	        // At beginning e(cursor) will point to 
	        // index just before the first element in v 
	        Enumeration e =  vForVector.elements(); 
	  
	        // Checking the next element availability 
	        while (e.hasMoreElements()) 
	        { 
	            // moving cursor to next element 
	            int i = (Integer)e.nextElement(); 
	  
	            System.out.print(i + " "); 
	        } 
	    } 
}
