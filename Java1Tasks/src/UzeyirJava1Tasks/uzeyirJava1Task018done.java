package UzeyirJava1Tasks;

import java.util.Arrays;

import src.ArrayList;
import src.String;

public class uzeyirJava1Task018done {

	public static void main(String[] args) {
		//In your framework, share your code that has ArrayList
		  private Arrays<String> groceryList = new ArrayList<String>();

		    public void addGroceryItem(String item) {
		        groceryList.add(item);
		    }

		    public void printGroceryList() {
		        System.out.println("You have " + groceryList.size() + " items in your grocery list");
		        for(int i=0; i< groceryList.size(); i++) {
		            System.out.println((i+1) + ". " + groceryList.get(i));
		        }
		    }

		    public void modifyGroceryItem(int position, String newItem) {
		        groceryList.set(position, newItem);
		        System.out.println("Grocery item " + (position+1) + " has been modified.");
		    }

		    public void removeGroceryItem(int position) {
		        String theItem = groceryList.get(position);
		        groceryList.remove(position);
		    }
	}

}
