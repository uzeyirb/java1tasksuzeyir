package UzeyirJava1Tasks;

import java.util.TreeSet;

public class uzeyirJava1Task022Done {
//Task022	In your framework, share your code that has TreeSet
	public static void main(String[] args) {
		  TreeSet<String> PCSet = new TreeSet<String>(); 
		  
	        // Elements are added using add() method 
		  PCSet.add("AppleMac"); 
		  PCSet.add("Dell"); 
		  PCSet.add("Asus"); 
	  
	        // Duplicates will not get insert 
		  PCSet.add("Asus"); 
	  
	        // Elements get stored in default natural 
	        // Sorting Order(Ascending) 
	        System.out.println(PCSet); 

	}

}
