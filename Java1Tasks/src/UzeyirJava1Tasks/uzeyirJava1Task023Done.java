package UzeyirJava1Tasks;

import java.util.HashMap;
import java.util.Map;

public class uzeyirJava1Task023Done {
//Task023	In your framework, share your code that has HashMap
	public static void main(String[] args) {

		HashMap<String, Integer> PetMap = new HashMap<>();

		print(PetMap);
		PetMap.put("dog", 10);
		PetMap.put("cat", 30);
		PetMap.put("bird", 20);

		System.out.println("Size of map is:- " + PetMap.size());

		print(PetMap);
		if (PetMap.containsKey("fish")) {
			Integer a = PetMap.get("fish");
			System.out.println("value for key" + " \"fish\" is:- " + a);
		}

		PetMap.clear();
		print(PetMap);
	}

	public static void print(Map<String, Integer> map) {
		if (map.isEmpty()) {
			System.out.println("map is empty");
		}

		else {
			System.out.println(map);
		}
	}
}
