package UzeyirJava1Tasks;

public class uzeyirJava1Task015Done {
	public class Vehicle {

		private String name;
		private String size; // body type

		private int currentSpeed;
		private int currentDirection;

		public Vehicle(String name, String size) {
			super();
			this.name = name;
			this.size = size;
			this.currentSpeed = 0;
			this.currentDirection = 0;
		}

		public int getCurrentSpeed() {
			return currentSpeed;
		}

		public void setCurrentSpeed(int currentSpeed) {
			this.currentSpeed = currentSpeed;
		}

		public int getCurrentDirection() {
			return currentDirection;
		}

		public void setCurrentDirection(int currentDirection) {
			this.currentDirection = currentDirection;
		}

		public void steer(int direction) {
			this.currentDirection += direction;
			System.out.println("Vehicle.steer(): Steering at " + currentDirection + " degrees.");
		}

		public void move(int speed, int direction) {
			currentSpeed = speed;
			currentDirection = direction;
			System.out.println("Vehicle.move(): Moving at " + currentSpeed + "in direction " + currentDirection);
		}

		public void stop() {
			this.currentSpeed = 0;
		}
	}

	public class Car extends Vehicle {

		private int wheels;
		private int doors;
		private int gears;
		private boolean isManual;
		private int currentGear;

		public Car(String name, String size, int wheels, int doors, int gears, boolean isManual) {
			super(name, size);
			this.wheels = wheels;
			this.doors = doors;
			this.gears = gears;
			this.isManual = isManual;
			this.currentGear = 1;
		}

		public void changeGear(int currentGear) {
			this.currentGear = currentGear;
			System.out.println("Car.changeGear(): Changed to " + this.currentGear + "gear.");
		}

		public void changeSpeed(int speed, int direction) {
			move(speed, direction);
			System.out.println("Car.changeVelocity(): Velocity " + speed + " direction" + direction);
		}

		@Override
		public void stop() {
			// TODO Auto-generated method stub
			super.stop();
		}

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
