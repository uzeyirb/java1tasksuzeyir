package UzeyirJava1Tasks;

import java.util.ArrayList;
import java.util.ListIterator;

public class uzeyirJava1Task027done {

	public static void main(String[] args) 
    { 
        ArrayList<String> list = new ArrayList<String>(); 
  
        list.add("A"); 
        list.add("Z"); 
        list.add("E"); 
        list.add("R"); 
        list.add("B"); 
        list.add("A");
        list.add("Y");
        list.add("C");
        list.add("A");
        list.add("N");
        // ListIterator to traverse the list 
        ListIterator iterator = list.listIterator(); 
  
      
        System.out.println("Displaying list elements in forward direction : "); 
  
        while (iterator.hasNext()) 
            System.out.print(iterator.next() + " "); 
  
        System.out.println(); 
  
        
        System.out.println("Displaying list elements in backward direction : "); 
  
        while (iterator.hasPrevious()) 
            System.out.print(iterator.previous() + " "); 
  
        System.out.println(); 
    } 

}
