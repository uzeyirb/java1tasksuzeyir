package UzeyirJava1Tasks;

import java.util.LinkedList;

public class uzeyirJava1Task020done {

	public static void main(String[] args) {
		
		LinkedList<String> productList = new LinkedList<String>(); 
		  
        
        productList.add("A"); 
        productList.add("B"); 
        productList.addLast("C"); 
        productList.addFirst("D"); 
        
        System.out.println("Linked list : " + productList); 
  
       
        productList.remove("B"); 
        productList.remove(3); 
        productList.removeFirst(); 
        productList.removeLast(); 
        System.out.println("Linked list after deletion: " + productList); 
  
         
        boolean status = productList.contains("E"); 
  
        if(status) 
            System.out.println("List contains the element 'E' "); 
        else
            System.out.println("List doesn't contain the element 'E'"); 
  
       
        int size = productList.size(); 
        System.out.println("Size of linked list = " + size); 
  
        
        Object element = productList.get(2); 
        System.out.println("Element returned by get() : " + element); 
        productList.set(2, "Y"); 
        System.out.println("Linked list after change : " + productList);

	}

}
