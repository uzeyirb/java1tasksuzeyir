package UzeyirJava1Tasks;
// The finally block always executes when the try block exits.
// This ensures that the finally block is executed even if an unexpected exception occurs.
final public class uzeyirJava1Task002Done {
//Final class can not be extended
	public static void main(String[] args) {
		// Task002
		//In your framework, use final and finally and write the locations of them
		
		//In the example below when we divide 100 by 0 it will
		// any integer divide by zero will give infinity and 
		//Java will throw ArithmeticException
		//Only in one case finally will not be 
		//executed when you have system.exit(1); inside your try block 
		
		try {
			
			int a=100/0;
			
		} catch (ArithmeticException e) {
			// TODO: handle exception
			System.out.println("Catch called");
			System.out.println(e);
		}finally {
			System.out.println("finally called not matter what, except if system.exit method is present");
		}

	}
	
	final public boolean printConfirmed() {
		//Final method cannot be overridden
		return false;
	}

}
