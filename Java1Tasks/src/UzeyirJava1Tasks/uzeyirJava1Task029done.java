package UzeyirJava1Tasks;

import java.util.ArrayList;
import java.util.Collections;

public class uzeyirJava1Task029done {
//In your framework, share your code that sorts an ArrayList
	public static void main(String args[]){
		   ArrayList<Integer> arraylist = new ArrayList<Integer>();
		   arraylist.add(11);
		   arraylist.add(2);
		   arraylist.add(7);
		   arraylist.add(3);
		   /* ArrayList before the sorting*/
		   System.out.println("Mixed:");
		   for(int counter: arraylist){
				System.out.println(counter);
			}

		   /* Sorting of arraylist using Collections.sort*/
		   Collections.sort(arraylist);

		   /* ArrayList after sorting*/
		   System.out.println("Sorted:");
		   for(int counter: arraylist){
				System.out.println(counter);
			}
}}
