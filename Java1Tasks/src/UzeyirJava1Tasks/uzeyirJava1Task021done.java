package UzeyirJava1Tasks;

import java.util.HashSet;
import java.util.Iterator;

public class uzeyirJava1Task021done {
	//Task021	In your framework, share your code that has HashSet

	public static void main(String[]args) 
    { 
        HashSet<String> carParts = new HashSet<String>(); 
  
         
        carParts.add("Bumpers"); 
        carParts.add("Fenders"); 
        carParts.add("Radiator"); 
        carParts.add("Mirror");
  
        
        System.out.println(carParts); 
        System.out.println("List contains India or not:" + 
        		carParts.contains("India")); 
  
       
        carParts.remove("Australia"); 
        System.out.println("List after removing Australia:"+carParts); 
  
      
        System.out.println("Iterating over list:"); 
        Iterator<String> i = carParts.iterator(); 
        while (i.hasNext()) 
            System.out.println(i.next()); 
    } 

}
