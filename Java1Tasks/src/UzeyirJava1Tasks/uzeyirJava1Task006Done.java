package UzeyirJava1Tasks;

public class uzeyirJava1Task006Done {

	
		// In your framework, override at least 2 constructors and write the locations of them

		private String number;
		private double balance;
		private String customerName;
		private String customerPhoneNumber;
		private String customerEmail;

		public uzeyirJava1Task006Done() {
			
			System.out.println("Empty constructor called");

		}

		public uzeyirJava1Task006Done(String number, double balance, String customerName, String customerPhoneNumber) {
		
			System.out.println("user account constructor with parameters called");

		}
		
		public uzeyirJava1Task006Done(String number, double balance, String customerName, String customerPhoneNumber, String customerEmail) {
			System.out.println("user account constructor with additional parameters called");
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}

		public double getBalance() {
			return balance;
		}

		public void setBalance(double balance) {
			this.balance = balance;
		}

		public String getCustomerName() {
			return customerName;
		}

		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}

		public String getCustomerPhoneNumber() {
			return customerPhoneNumber;
		}

		public void setCustomerPhoneNumber(String customerPhoneNumber) {
			this.customerPhoneNumber = customerPhoneNumber;
		}

		public String getCustomerEmail() {
			return customerEmail;
		}

		public void setCustomerEmail(String customerEmail) {
			this.customerEmail = customerEmail;
		}

	

}
