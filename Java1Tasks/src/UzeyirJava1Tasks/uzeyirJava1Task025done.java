package UzeyirJava1Tasks;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class uzeyirJava1Task025done {
//In your framework, share your code that has TreeMap
	public static void main(String[] args) {
		/* This is how to declare TreeMap */
		TreeMap<Integer, String> MTR = new TreeMap<Integer, String>();

		/* Adding elements to TreeMap */
		MTR.put(1, "Table1");
		MTR.put(23, "Table2");
		MTR.put(70, "Table3");
		MTR.put(4, "Table4");
		MTR.put(2, "Table5");

		/* Display content using Iterator */
		Set set = MTR.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry mentry = (Map.Entry) iterator.next();
			System.out.print("key is: " + mentry.getKey() + " & Value is: ");
			System.out.println(mentry.getValue());
		}
	}
}
